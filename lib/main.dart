import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() => runApp(RGStarterKit());

class RGStarterKit extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Riskgratis',
      theme: ThemeData(
        primaryColor: Colors.white,
        textTheme: Theme.of(context).textTheme.apply(displayColor: Colors.grey[900]),
        fontFamily: 'Raleway',
      ),
      home: SafeArea(child: Dashboard(title: 'DASHBOARD'),),
    );
  }
}

class Dashboard extends StatefulWidget {
  Dashboard({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  final List<dynamic> tasks = new List<dynamic>();
  final List<dynamic> products = new List<dynamic>();
  final List<dynamic> availableProduct = new List<dynamic>();
  int _selectedMenuIndex = 0;
 
  @override
    void initState() {
      super.initState();

      this.setState(() {
        this.tasks.addAll([
          {'icon': 'icons8-notification-96.png', 'text': 'NOTIFICATION', 'color': Colors.grey[800]},
          {'icon': 'icons8-wheelchair-96.png', 'text': 'CLAIMS', 'color': Colors.grey[800]},
          {'icon': 'icons8-shopping-bag-96.png', 'text': 'POLICY', 'color': Colors.grey[800]},
          {'icon': 'icons8-potted-plant-96.png', 'text': 'PREMIUM', 'color': Colors.grey[800]},
          {'icon': 'icons8-increase-96.png', 'text': 'AMB GRAPH', 'color': Colors.grey[800]},
        ]);

        this.products.addAll([
          {'icon': 'icons8-kidney-96.png', 'text': 'Health', 'color': Colors.blue[900]},
          {'icon': 'icons8-campfire-96.png', 'text': 'Fire & Peril', 'color': Colors.red[900]},
          {'icon': 'icons8-convertible-96.png', 'text': 'Motor', 'color': Colors.teal[700]},
          {'icon': 'icons8-water-transportation-96.png', 'text': 'Marine', 'color': Colors.deepPurpleAccent[700]},
          {'icon': 'icons8-new-job-96.png', 'text': 'Professional Indemnity', 'color': Colors.yellowAccent[400]},
          {'icon': 'icons8-airplane-take-off-96.png', 'text': 'Travel', 'color': Colors.blueGrey[900]},
          {'icon': 'icons8-home-96.png', 'text': 'Home', 'color': Colors.cyanAccent[700]},
          {'icon': 'icons8-in-transit-96.png', 'text': 'Goods In Transit', 'color': Colors.blue[900]},
        ]);

        this.availableProduct.addAll([
          {'icon': 'icons8-money-box-96.png', 'text': 'SAVINGS PLAN', 'color': Color(0xfff7bbcf)},
          {'icon': 'icons8-reading-96.png', 'text': 'EDUCATION PLAN', 'color': Color(0xff5b6abe)},
          {'icon': 'icons8-coffin-96.png', 'text': 'MEMORIAL PLAN', 'color': Color(0xff967A44)},
          {'icon': 'icons8-lease-96.png', 'text': 'MORTGAGE PLAN', 'color': Color(0xffDD872A)},
          {'icon': 'icons8-user-shield-96.png', 'text': 'PROTECTION PLAN', 'color': Color(0xff673AB7)},
          {'icon': 'icons8-holiday-96.png', 'text': 'RETIREMENT PLAN', 'color': Color(0xff00BCD4)},
        ]);
      });
    }


  @override
  Widget build(BuildContext context) {

    List<Widget> wdg = [
      this.createSearchBar(), 
      this.createUserTask(context), 
      this.createFeaturedProduct(context), 
      this.createAvailableProductHeader(),
      this.createAvailableProduct()
    ];

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.menu), onPressed: () => null,),
        title: Center(child: Text(widget.title, style: TextStyle(fontFamily: 'RalewayExtraBold', fontWeight: FontWeight.bold,),),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.share), onPressed: () => null,)
        ],
        elevation: 0.0,
      ),
      body: ListView.builder(
        itemCount: wdg.length,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) => wdg[index]
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(FontAwesomeIcons.thLarge, color: Colors.black), title: Text('Dashboard', style: TextStyle(color: Colors.black),)),
          BottomNavigationBarItem(icon: Icon(FontAwesomeIcons.cog, color: Colors.blue[900],), title: Text('Chat', style: TextStyle(color: Colors.black),)),
          BottomNavigationBarItem(icon: Icon(FontAwesomeIcons.comments, color: Colors.red[900],), title: Text('Settings', style: TextStyle(color: Colors.black),)),
          BottomNavigationBarItem(icon: Icon(FontAwesomeIcons.userNinja, color: Colors.red[900],), title: Text('Account', style: TextStyle(color: Colors.black),)),
        ],
        onTap: this._onTapItemBottomNavigationBar,
        currentIndex: this._selectedMenuIndex,
        fixedColor: Colors.black,
      ),
    );
  }

  void _onTapItemBottomNavigationBar(int index) {
    setState(() {
      _selectedMenuIndex = index;
    });
  }

  Material createSearchBar() {
    return Material(
      elevation: 0.0,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        child: TextFormField(
          decoration: InputDecoration(
            hasFloatingPlaceholder: false,
            isDense: true,
            border: InputBorder.none,
            hintText: 'Search RISKGRATIS Software',
            contentPadding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 5.0),
            prefixIcon: Icon(Icons.search),
            suffixIcon: Icon(Icons.mic),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20.0), 
              borderSide: BorderSide(width: 1.0, style: BorderStyle.solid, color: Colors.grey[300] )
            ),
          ),
        ),
      ),
    );
  }


  Container createUserTask(BuildContext context) {
    return this.createGenericLeftSlider(context, this.tasks);
  }


  Column createFeaturedProduct(BuildContext context) {
    double iconWidth = 100.0;
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container( 
          margin: EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: Text('FEATURED', style: TextStyle(fontWeight: FontWeight.bold),),
              ),
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Text('LINKAGE ASSURANCE LIMITED', style: TextStyle(fontWeight: FontWeight.w100, fontSize: 12.0),),
              ),
            ],
          ),
        ),
        Container(
          child: this.createGenericLeftSlider(context, this.products, iconWidth, false),
        ),
      ],
    );
  }

  Container createAvailableProductHeader() {
    return Container(
      margin: EdgeInsets.only(top: 5.0, bottom: 0.0, left: 8.0),
      child: Text('AVAILABLE PRODUCTS', style:TextStyle(fontWeight: FontWeight.bold)),
    );
  }


  GridView createAvailableProduct() {
    return  GridView.count(
      crossAxisCount: 3,
      shrinkWrap: true,
      mainAxisSpacing: 0.0,
      crossAxisSpacing: 10.0,
      childAspectRatio: 1.0,
      padding: EdgeInsets.all(0.0),
      children: List.generate(this.availableProduct.length, (int index) => 
        Center(
          child: InkWell(
            onTap: () => null,
            splashColor: this.availableProduct[index]['color'],
            child: Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                Container(
                  width: 60.0,
                  height: 60.0,
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: this.availableProduct[index]['color'], width: 1.0, style: BorderStyle.solid),
                    borderRadius: BorderRadius.circular(200.0),
                  ),
                  child: SizedBox(width: 30.0, height: 30.0, child: Image.asset('images/${this.availableProduct[index]['icon']}'),),
                ),
                Positioned(
                  top: 50.0,
                  left: -12.0,
                  child: Container(
                    width: 86.0,
                    padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 3.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100.0),
                      color: this.availableProduct[index]['color'],
                    ), 
                    child: Center(
                      child: Text(this.availableProduct[index]['text'], style: TextStyle(fontSize: 10.0, color: Colors.white, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget createGenericLeftSlider(BuildContext context, List<dynamic> list, [ double iconWidth = 100.0, bool border = true ]) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 85.0,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
      decoration: border ? BoxDecoration(border: Border(bottom: BorderSide(width: 1.0, style: BorderStyle.solid, color: Colors.grey[300])),) : null,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: List.generate(list.length, (int index) => 
          InkWell(
            onTap: () => null,
            splashColor: Colors.teal[100],
            child: Container ( 
              width: iconWidth,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(width: 35.0, height: 35.0, child: Image.asset('images/${list[index]['icon']}', fit: BoxFit.cover,),),
                  Padding(padding: EdgeInsets.symmetric(vertical: 5.0),),
                  Center(
                    child: Text(list[index]['text'],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold, 
                        fontSize: 10.0,
                        color: Colors.grey[700],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      )
    );
  }
}
